//
//  ViewController.h
//  MembersPaymentsManager
//
//  Created by Ntambwa Basambombo on 2014-09-23.
//  Copyright (c) 2014 Basa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Palette.h"
#import "Color.h"

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *roundCircle;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

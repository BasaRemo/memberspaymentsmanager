//
//  ViewController.m
//  MembersPaymentsManager
//
//  Created by Ntambwa Basambombo on 2014-09-23.
//  Copyright (c) 2014 Basa. All rights reserved.
//

#import "ViewController.h"

#define HEXColor(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.button setBackgroundImage:[Color roundImageForSize:self.button.frame.size withColor:HEXColor(0x1dacd6)] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
